let htmlBody = document.querySelector(`body`);
htmlBody.style.boxSizing = "border-box";
htmlBody.style.margin = "0%";

let mainDiv = document.createElement("div");
htmlBody.append(mainDiv);
mainDiv.style.width = "100%";
mainDiv.style.height = "1800px";
mainDiv.style.backgroundColor = "red";
let heading = document.createElement("header");
heading.style.width = "100%";
heading.style.height = "5%";
heading.style.backgroundColor = "green";
heading.innerText = "Grid Color";
heading.style.fontSize = "40px";
heading.style.textAlign = "center";
heading.style.padding = "10px 0";
mainDiv.append(heading);
let subDiv = document.createElement("div");
mainDiv.append(subDiv);

for (let index = 1; index <= 500; index++) {
  let innerdiv = document.createElement("div");
  innerdiv.classList.add("child");
  innerdiv.style.width = "4rem";
  innerdiv.style.height = "4rem";
  innerdiv.style.margin = "0";
  innerdiv.style.border = "1px solid black";
  innerdiv.style.display = "inline-block";
  innerdiv.innerText = index;
  subDiv.append(innerdiv);
}
let childDiv = document.querySelectorAll(`.child`);
subDiv.addEventListener("mouseover", function () {
  for (let index = 0; index < childDiv.length; index++) {
    childDiv[index].textContent = Math.floor(Math.random() * childDiv.length);
    childDiv[index].style.backgroundColor =
      "#" + Math.floor(Math.random() * 16777215).toString(16);
    heading.style.background = "black";
    heading.style.color = "white";
  }
});

heading.addEventListener("mouseover", function () {
  for (let index = 0; index < childDiv.length; index++) {
    childDiv[index].style.background = "white";
    childDiv[index].textContent = index;
    heading.style.backgroundColor = "green";
  }
});
